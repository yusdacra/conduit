{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.d2n.url = "github:nix-community/dream2nix";
  inputs.d2n.inputs.nixpkgs.follows = "nixpkgs";
  inputs.parts.url = "github:hercules-ci/flake-parts";
  inputs.rust-overlay.url = "github:oxalica/rust-overlay";
  inputs.rust-overlay.inputs.nixpkgs.follows = "nixpkgs";

  outputs = inp:
    inp.parts.lib.mkFlake {inputs = inp;} {
      systems = ["x86_64-linux"];
      imports = [inp.d2n.flakeModuleBeta];
      perSystem = {
        config,
        system,
        pkgs,
        ...
      }: let
        cargoToml = builtins.fromTOML (builtins.readFile ./Cargo.toml);
        pkgsWithToolchain = pkgs.appendOverlays [inp.rust-overlay.overlays.default];

        toolchains = pkgsWithToolchain.rust-bin.stable."${cargoToml.package.rust-version}";
        # toolchain to use when building conduit, includes only cargo and rustc to reduce closure size
        buildToolchain = toolchains.minimal;
        # toolchain to use in development shell
        # the "default" component set of toolchain adds rustfmt, clippy etc.
        devToolchain = toolchains.default.override {
          extensions = ["rust-src"];
        };
        # rust hooks from nixpkgs, we need the bindgenHook from here
        rustHooks = pkgs.callPackage "${pkgs.path}/pkgs/build-support/rust/hooks" {
          cargo = buildToolchain;
          rustc = buildToolchain;
          stdenv = pkgs.stdenv;
        };

        # flake outputs for conduit project
        conduitOutputs = config.dream2nix.outputs.conduit;
      in {
        dream2nix.inputs.conduit = {
          source = inp.self;
          projects.conduit = {
            name = "conduit";
            subsystem = "rust";
            translator = "cargo-lock";
            builder = "crane";
          };
          packageOverrides = rec {
            "^.*".set-toolchain.overrideRustToolchain = _: {
              cargo = buildToolchain;
              rustc = buildToolchain;
            };
            "conduit-deps" = {
              add-rocks-db = {
                ROCKSDB_INCLUDE_DIR = "${pkgs.rocksdb_6_23}/include";
                ROCKSDB_LIB_DIR = "${pkgs.rocksdb_6_23}/lib";
              };
              add-bindgen-hook.overrideAttrs = old: {
                nativeBuildInputs =
                  (old.nativeBuildInputs or [])
                  ++ [
                    rustHooks.bindgenHook
                  ];
              };
            };
            "conduit" = {
              inherit (conduit-deps) add-rocks-db add-bindgen-hook;
              disable-tests-on-darwin = {
                # tests failed on x86_64-darwin with SIGILL: illegal instruction
                doCheck = !(pkgs.stdenv.isx86_64 && pkgs.stdenv.isDarwin);
              };
            };
          };
        };
        packages.conduit = conduitOutputs.packages.conduit;
        packages.default = config.packages.conduit;
        devShells.conduit = conduitOutputs.devShells.conduit.overrideAttrs (old: {
          # export default crate sources for rust-analyzer to read
          RUST_SRC_PATH = "${devToolchain}/lib/rustlib/src/rust/library";
          nativeBuildInputs =
            (old.nativeBuildInputs or [])
            ++ [
              (pkgs.lib.hiPrio devToolchain)
            ];
        });
        devShells.default = config.devShells.conduit;
      };
    };
}
